@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (Session::has('message'))
            <div class="alert alert-success" role="alert">
                {{Session::get('message')}}
            </div>
            @endif

            {{-- <a class="btn btn-xs btn-success" href="{{route('create-classroom')}}">Add classroom</a> --}}
            <br>
            <br>
            <div class="card">
                <div class="card-header">{{$classRoomData->class_name}} | {{$classRoomData->teacher_name}}</div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Student Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($classRoomStudentData as $key => $item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->student_name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection