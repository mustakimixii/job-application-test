@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (Session::has('message'))
            <div class="alert alert-success" role="alert">
                {{Session::get('message')}}
            </div>
            @endif

            <a class="btn btn-xs btn-success" href="{{route('create-classroom')}}">Add classroom</a>
            <br>
            <br>
            <div class="card">
                <div class="card-header">{{ __('List classroom') }}</div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Class Name</th>
                                <th scope="col">Teacher Name</th>
                                <th scope="col" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($classroomList as $key => $item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->class_room_name}}</td>
                                <td>{{$item->user_name}}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-success"
                                        href="{{route('show-classroom',['id'=>$item->id])}}">View</a>
                                    <a class="btn btn-xs btn-primary"
                                        href="{{route('edit-classroom',['id'=>$item->id])}}">Edit</a>
                                    <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure?')"
                                        href="{{route('delete-classroom',['id'=>$item->id])}}">Delete</a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection