@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (Session::has('error'))
        <div class="alert alert-danger" role="alert">
            {{Session::get('error')}}
        </div>
        @endif
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Classroom Data ') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('update-classroom',['id'=>$classRoomData->id]) }}"
                        aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                    class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                    value="@if($classRoomData->name){{$classRoomData->name}}@else{{ old('name') }}@endif"
                                    required autofocus>
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="teacher"
                                    class="col-md-4 col-form-label text-md-right">{{ __('teacher') }}</label>
    
                                <div class="col-md-6">
                                    <select name="teacher"
                                        class="form-control{{ $errors->has('teacher') ? ' is-invalid' : '' }}" id="teacher">
                                        <option value="" disabled selected>--- Select Teacher -- </option>
                                        @foreach ($teacherList as $item)
                                        <option value="{{$item->id}}"  @if($item->id==$classRoomData->teacher_id) selected @endif>--- {{$item->name}} -- </option>
                                        @endforeach
                                    </select>
    
                                    @if ($errors->has('teacher'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('teacher') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="student"
                                    class="col-md-4 col-form-label text-md-right">{{ __('student') }}</label>
                                <div class="col-md-6">
                                    <select class="js-example-basic-multiple form-control" multiple="multiple"
                                        name="student[]"
                                        class="form-control{{ $errors->has('student') ? ' is-invalid' : '' }}" id="student">
                                        @foreach ($studentList as $item)
                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                        @endforeach
                                    </select>
    
                                    @if ($errors->has('student'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('student') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
    
                            <script src="https://code.jquery.com/jquery-3.1.1.js"
                            integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
                        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    
                            <script>
                                $(document).ready(function () {
                                    $('.js-example-basic-multiple').select2();
                                });
                            </script>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection