<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "admin",
            'role_id' => "1",
            'email' => 'admin@gg.com',
            'password' => bcrypt('admin123'),
        ]);
    }
}
