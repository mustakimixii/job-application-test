<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([[
            'id' => "1",
            'name' => "admin"
        ],
        [
            'id' => "2",
            'name' => "teacher"
        ],
        [
            'id' => "3",
            'name' => "student"
        ]]);
    }
}
