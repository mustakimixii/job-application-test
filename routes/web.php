<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Class Room CRUD
Route::get('/classroom', 'ClassRoomController@index')->name('classroom-index');
Route::get('/classroom/create', 'ClassRoomController@create')->name('create-classroom');
Route::post('/classroom/store', 'ClassRoomController@store')->name('store-classroom');
Route::get('/classroom/show/{id}', 'ClassroomController@show')->name('show-classroom');
Route::get('/classroom/edit/{id}', 'ClassRoomController@edit')->name('edit-classroom');
Route::post('/classroom/update/{update}', 'ClassRoomController@update')->name('update-classroom');
Route::get('/classroom/delete/{id}', 'ClassRoomController@destroy')->name('delete-classroom');


//Student CRUD
Route::get('/student', 'StudentController@index')->name('student-index');
Route::get('/student/getdata', 'StudentController@getData')->name('get-data-index');
Route::get('/student/create', 'StudentController@create')->name('create-student');
Route::post('/student/store', 'StudentController@store')->name('store-student');
Route::get('/student/show/{id}', 'StudentController@show')->name('show-student');
Route::get('/student/edit/{id}', 'StudentController@edit')->name('edit-student');
Route::post('/student/update/{update}', 'StudentController@update')->name('update-student');
Route::get('/student/delete/{id}', 'StudentController@destroy')->name('delete-student');


//Teacher CRUD
Route::get('/teacher', 'TeacherController@index')->name('teacher-index');
Route::get('/teacher/create', 'TeacherController@create')->name('create-teacher');
Route::post('/teacher/store', 'TeacherController@store')->name('store-teacher');
Route::get('/teacher/show/{id}', 'TeacherController@show')->name('show-teacher');
Route::get('/teacher/edit/{id}', 'TeacherController@edit')->name('edit-teacher');
Route::post('/teacher/update/{update}', 'TeacherController@update')->name('update-teacher');
Route::get('/teacher/delete/{id}', 'TeacherController@destroy')->name('delete-teacher');
