<?php

namespace App\Http\Controllers;

use App\StudentClassRoom;
use Illuminate\Http\Request;

class StudentClassRoomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StudentClassRoom  $studentClassRoom
     * @return \Illuminate\Http\Response
     */
    public function show(StudentClassRoom $studentClassRoom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StudentClassRoom  $studentClassRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentClassRoom $studentClassRoom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StudentClassRoom  $studentClassRoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentClassRoom $studentClassRoom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StudentClassRoom  $studentClassRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentClassRoom $studentClassRoom)
    {
        //
    }
}
