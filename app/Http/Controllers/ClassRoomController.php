<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\StudentClassRoom;
use App\Models\User;
use Illuminate\Http\Request;
use Log;
use DB;

class ClassRoomController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classRoomList=ClassRoom::select('class_rooms.id as id', 'users.name as user_name', 'class_rooms.name as class_room_name')->join('users', 'users.id', '=', 'class_rooms.teacher_id')->paginate(20);
        // return $classRoomList;
        return view('classroom.index', ['classroomList'=>$classRoomList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacherList=User::with('roles')->whereRoleId('2')->get();
        $studentList=User::with('roles')->whereRoleId('3')->get();
        return view('classroom.create', ['teacherList'=>$teacherList,'studentList'=>$studentList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $studentId= $request->get('student');
        try {
            ClassRoom::create(["name" => $request->get('name'),"teacher_id" => $request->get('teacher')]);
            for ($i=0; $i < \sizeof($studentId); $i++) {
                StudentClassRoom::create(["class_room_id" => ClassRoom::max('id'),"student_id" => $studentId[$i]]);
            }
            return redirect('classroom')->with(['message'=>'Data Saved Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Save Data Failed!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassRoom  $classRoom
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classRoomData = User::select('users.name as teacher_name', 'class_rooms.name as class_name')->join('class_rooms', 'class_rooms.teacher_id', '=', 'users.id')
        ->where('class_rooms.id', $id)
        ->first();
        
        $classRoomStudentData = User::select('users.name as student_name')->join('student_class_rooms', 'student_class_rooms.student_id', '=', 'users.id')
        ->where('student_class_rooms.class_room_id', $id)
        ->get();

        // return ['classRoomData'=>$classRoomData,'classRoomStudentData'=>$classRoomStudentData];
        return view('classroom.show', ['classRoomData'=>$classRoomData,'classRoomStudentData'=>$classRoomStudentData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassRoom  $classRoom
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classRoomData = ClassRoom::whereId($id)->first();
        $teacherList=User::with('roles')->whereRoleId('2')->get();
        $studentList=User::select('users.name as name', 'users.id as id')->join('student_class_rooms', 'student_class_rooms.student_id', '=', 'users.id')
        ->where('student_class_rooms.class_room_id', $id)
        ->get();

        return view('classroom.edit', ['classRoomData'=>$classRoomData,'teacherList'=>$teacherList,'studentList'=>$studentList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassRoom  $classRoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $newStudentData= $request->get('student');
        $currentStudentData = StudentClassRoom::where('class_room_id', $id)->get();
        $currentStudentDataDump=[];
        
        try {
            // return 
            $classRoomData = ClassRoom::whereId($id)->first();
            $classRoomData->name=$request->get('name');
            $classRoomData->teacher_id=$request->get('teacher');
            $classRoomData->update();
            // dd($classRoomData);
            foreach ($currentStudentData as $value) {
                array_push($currentStudentDataDump, $value->student_id);
                
                if (in_array($value->student_id, $newStudentData)) {
                } else {
                    DB::table('student_class_rooms')->where([['class_room_id','=',$id],['student_id','=',$value->student_id]])->delete();
                }
            }
            for ($i=0; $i < \sizeof($newStudentData); $i++) {
                $studentClassRoomData=StudentClassRoom::where([['class_room_id','=',$id],['student_id','=',$value->student_id]])->first();
                if (!$studentClassRoomData) {
                    StudentClassRoom::create(["class_room_id" => $id,"student_id" => $newStudentData[$i]]);
                }
            }
                
            return redirect('classroom')->with(['message'=>'Data Updated Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data Updated Failed']);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param \App\ClassRoom $classRoom
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        try {
            $classroomData = ClassRoom::whereId($id)->first();
            $classroomData->delete($id);
            return redirect('classroom')->with(['message'=>'Data deleted Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data deleted Successfully Failed']);
        }
    }
}
