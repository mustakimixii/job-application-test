<?php

namespace App\Http\Controllers;

use App\Models\User;;

use App\Http\Requests\TeacherRequest;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacherList=User::with('roles')->whereRoleId('2')->paginate(20);
        return view('teacher.index', ['teacherList'=>$teacherList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            User::create(["name" =>$request->get('name'),"role_id"=>2]);
            return redirect('teacher')->with(['message'=>'Data Saved Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->withMessage('Save Data Failed!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $teacher, $id)
    {
        $teacherData = User::whereId($id)->first();
        return view('teacher.edit', ['teacherData'=>$teacherData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, $id)
    {
        try {
            $teacherData = User::whereId($id)->first();
            $teacherData->name=$request->get('name');
            $teacherData->update();
            return redirect('teacher')->with(['message'=>'Data Updated Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data Updated Failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $teacherData = User::whereId($id)->first();
            $teacherData->delete($id);
            return redirect('teacher')->with(['message'=>'Data deleted Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data deleted Successfully Failed']);
        }
    }
}
