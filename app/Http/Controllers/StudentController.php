<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;

use App\Http\Requests\StudentRequest;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentList=User::with('roles')->whereRoleId('3')->paginate(20);
        return view('student.index', ['studentList'=>$studentList]);
    }


    public function getSataStudentAjax()
    {
        return $studentList=User::with('roles')->whereRoleId('3')->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        try {
            User::create(["name" =>$request->get('name'),"role_id"=>3]);
            return redirect('student')->with(['message'=>'Data Saved Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->withMessage('Save Data Failed!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $studentData = User::whereId($id)->first();
        return view('student.edit', ['studentData'=>$studentData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $student, $id)
    {
        $studentData = User::whereId($id)->first();
        return view('student.edit', ['studentData'=>$studentData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $studentData = User::whereId($id)->first();
            $studentData->name=$request->get('name');
            $studentData->update();
            return redirect('student')->with(['message'=>'Data Updated Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data Updated Failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        try {
            $studentData = User::whereId($id)->first();
            $studentData->delete($id);
            return redirect('student')->with(['message'=>'Data deleted Successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return redirect()->back()->with(['message'=>'Data deleted Successfully Failed']);
        }
    }
}
