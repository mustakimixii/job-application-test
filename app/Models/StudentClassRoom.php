<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentClassRoom extends Model
{
    protected $fillable = [
        'student_id', 'class_room_id'
    ];
    public function classRooms()
    {
        return $this->belongsTo('App\Models\ClassRoom');
    }
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
