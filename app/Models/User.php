<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','role_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function studentClassRooms()
    {
        return $this->hasMany('App\Models\StudentClassRoom');
    }
    public function classRooms()
    {
        return $this->hasMany('App\Models\ClassRoom');
    }
    public function roles()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
