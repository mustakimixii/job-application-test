<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','teacher_id',
    ];
    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function studentClassRooms()
    {
        return $this->hasMany('App\Models\StudentClassRoom');
    }
}
